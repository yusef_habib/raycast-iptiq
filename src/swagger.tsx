import { Action, ActionPanel, List, getPreferenceValues } from "@raycast/api";

function getSwaggerIssueLink(env: string, service: string) {
  const domain =
    env === "prod"
      ? "prod.p.eu1.caramelspec.com"
      : env === "prep"
        ? "prep.d.eu1.caramelspec.com"
        : "dint.d.eu1.caramelspec.com";

  return `https://${domain}/${service}/swagger-ui/index.html`;
}

export default function Command() {
  const { environment, servicesInput } = getPreferenceValues<Preferences>();
  const services = servicesInput.split(",");

  return (
    <List>
      <List.Section title="Service">
        {services.map((service) => (
          <List.Item
            key={service}
            title={service}
            actions={
              <ActionPanel>
                <Action.OpenInBrowser url={getSwaggerIssueLink(environment, service)} />
              </ActionPanel>
            }
          />
        ))}
      </List.Section>
    </List>
  );
}
