import { ActionPanel, List } from "@raycast/api";
import { useState } from "react";

import { DeleteHistory } from "./DeleteHistory";
import { SearchHistory, renderISODateAsDateWithTime, useLocalStorage } from "./utils";
import { OpenHeimdallLink } from "./OpenHeimdallLink";

export default function Command(props: { arguments: { env: string } }) {
  const env = props.arguments.env || "prod";
  const [search, setSearch] = useState<string>("");
  const { data, setData, isLoading } = useLocalStorage<SearchHistory[]>("history", []);

  return (
    <List
      navigationTitle="Search for a policyId"
      onSearchTextChange={setSearch}
      searchText={search}
      searchBarPlaceholder="Type and hit enter to open Heimdall"
      isLoading={isLoading}
      actions={
        <ActionPanel>
          <OpenHeimdallLink search={search} setData={setData} data={data} env={env} />
          <DeleteHistory setData={setData} />
        </ActionPanel>
      }
    >
      <List.Section title="Recent">
        {data.map(({ value, date }) => (
          <List.Item
            key={value}
            title={value}
            subtitle={`Added ${renderISODateAsDateWithTime(date)}`}
            actions={
              search.length === 0 ? (
                <ActionPanel>
                  <OpenHeimdallLink search={value} setData={setData} data={data} env={env} />
                  <DeleteHistory setData={setData} />
                </ActionPanel>
              ) : (
                <ActionPanel>
                  <OpenHeimdallLink search={search} setData={setData} data={data} env={env} />
                  <DeleteHistory setData={setData} />
                </ActionPanel>
              )
            }
          />
        ))}
      </List.Section>
    </List>
  );
}
