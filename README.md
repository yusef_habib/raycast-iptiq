# iptiQ

A colleciton of links and utilities for work. It is a WIP and will be updated as needed.

At the moment supports

- Heimdall: Provide a policyId to jump into its page or don't provide anything to jump into the main page.
- Jira: Provide a ticket number to jump into its page or select between Kanban board and Dashaboard.
- Swagger: Shortcuts to the swagger pages of the different services.
  - It is configurable to support multiple services.
- Loki: Shortcuts to the loki pages of the different services.
  - It is configurable to support multiple services.
  - ![loki-config](./assets/readme/loki-config.png)

## How to

A list of utility commands for work.

To run the extension locally:

- Close the repository
- Install dependencies with `npm install`
- Run the extension with `npm run dev` to install it locally. Right after, you can cancel the process and the extension will be still available.

You should be able to access all the commands in the extension.
