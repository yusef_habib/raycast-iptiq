import { Action, ActionPanel, List } from "@raycast/api";
import { useState } from "react";

const getJiraIssueLink = (query: string) => `https://jira.ss.eu1.caramelspec.com/browse/${query}`;
const boards = [
  { label: "Kanban", url: "https://jira.ss.eu1.caramelspec.com/secure/RapidBoard.jspa?rapidView=162&projectKey=DI" },
  { label: "Dashboard", url: "https://jira.ss.eu1.caramelspec.com/secure/Dashboard.jspa?selectPageId=13300" },
];

export default function Command() {
  const [search, setSearch] = useState("");

  return (
    <List onSearchTextChange={setSearch} searchText={search} searchBarPlaceholder="Type and hit enter a ticket number">
      <List.Section title="Service">
        {boards.map((board) => (
          <List.Item
            key={board.url}
            title={board.label}
            actions={
              <ActionPanel>
                <Action.OpenInBrowser url={search ? getJiraIssueLink(search) : board.url} />
              </ActionPanel>
            }
          />
        ))}
      </List.Section>
    </List>
  );
}
