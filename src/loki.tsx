import { List, ActionPanel, Action, getPreferenceValues } from "@raycast/api";
import { useState } from "react";

const getLokiUrl = (service: string, env: string, query: string = "") =>
  `https://grafana.ss.eu1.caramelspec.com/explore?schemaVersion=1&panes=%7B%223eO%22:%7B%22datasource%22:%220000000${env === "prod" ? "13" : "11"}%22,%22queries%22:%5B%7B%22expr%22:%22%7B%20container%3D%5C%22${service}%5C%22,%20namespace%3D%5C%22${env}%5C%22%20%7D%5Cn%7C%20json%5Cn%7C%3D%5C%22${query}%5C%22%5Cn%7C%20line_format%20%5C%22%7B%7B%20.message%20%7D%7D%5C%22%22,%22refId%22:%22A%22,%22range%22:true,%22editorMode%22:%22code%22,%22queryType%22:%22range%22,%22datasource%22:%7B%22type%22:%22loki%22,%22uid%22:%220000000${env === "prod" ? "13" : "11"}%22%7D%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1`;
const getAllErrorsLokiUrl = (services: string[], env: string) =>
  `https://grafana.ss.eu1.caramelspec.com/explore?schemaVersion=1&panes=%7B%22-iS%22:%7B%22datasource%22:%22000000013%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22%7B%20namespace%3D%5C%22${env}%5C%22,%20container%3D~%5C%22${encodeURI(services.join("|"))}%5C%22%20%7D%20%5Cn%7C%20json%20%5Cn%7C%20level%3D%60ERROR%60%20%5Cn%7C%20line_format%20%5C%22%7B%7B.mdc_traceId%7D%7D%5C%5Ct%7B%7B.application%7D%7D%5C%5Ct%7B%7B.message%7D%7D%5C%22%22,%22editorMode%22:%22code%22,%22queryType%22:%22range%22,%22datasource%22:%7B%22type%22:%22loki%22,%22uid%22:%22000000013%22%7D%7D%5D,%22range%22:%7B%22from%22:%22now-24h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1`;
const getIstioUrl = (query: string = "") =>
  `https://grafana.ss.eu1.caramelspec.com/explore?schemaVersion=1&panes=%7B%22i-G%22:%7B%22datasource%22:%22000000013%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22datasource%22:%7B%22type%22:%22loki%22,%22uid%22:%22000000013%22%7D,%22editorMode%22:%22code%22,%22expr%22:%22%7Bistio%3D%5C%22ingressgateway-external%5C%22%7D%20%5Cn%7C%3D%20%5C%22${query}%5C%22%5Cn%22,%22queryType%22:%22range%22,%22maxLines%22:5000%7D%5D,%22range%22:%7B%22from%22:%22now-24h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1`;
const getCertificateCreationEmail = () =>
  `https://grafana.ss.eu1.caramelspec.com/explore?schemaVersion=1&panes=%7B%223eO%22:%7B%22datasource%22:%22000000013%22,%22queries%22:%5B%7B%22expr%22:%22%7B%20container%3D%5C%22policy-motor%5C%22,%20namespace%3D%5C%22prod%5C%22%20%7D%5Cn%7C%20json%5Cn%7C%3D%5C%22Create%20certificate%20is%20requested%20by:%5C%22%5Cn%7C%20line_format%20%5C%22%7B%7B%20.message%20%7D%7D%5C%22%22,%22refId%22:%22A%22,%22range%22:true,%22editorMode%22:%22code%22,%22queryType%22:%22range%22,%22datasource%22:%7B%22type%22:%22loki%22,%22uid%22:%22000000013%22%7D%7D%5D,%22range%22:%7B%22from%22:%22now-7d%22,%22to%22:%22now%22%7D%7D%7D&orgId=1`;

export default function Command(props: { arguments: { env: string } }) {
  const environment = props.arguments.env || "prod";
  const { servicesInput } = getPreferenceValues<Preferences.Loki>();
  const services = servicesInput.split(",");
  const [search, setSearch] = useState("");

  return (
    <List
      onSearchTextChange={setSearch}
      searchText={search}
      searchBarPlaceholder="Type and hit enter to open Loki with a query"
    >
      <List.Section title="Service">
        {services.map((service) => (
          <List.Item
            key={service}
            title={service}
            actions={
              <ActionPanel>
                <Action.OpenInBrowser url={getLokiUrl(service, environment, search)} />
              </ActionPanel>
            }
          />
        ))}
        <List.Item
          key="istio"
          title="istio"
          actions={
            <ActionPanel>
              <Action.OpenInBrowser url={getIstioUrl(search)} />
            </ActionPanel>
          }
        />
        <List.Item
          key="all-errors"
          title="all errors"
          actions={
            <ActionPanel>
              <Action.OpenInBrowser url={getAllErrorsLokiUrl(services, environment)} />
            </ActionPanel>
          }
        />
        <List.Item
          key="certificate-owner"
          title="certificate creator"
          actions={
            <ActionPanel>
              <Action.OpenInBrowser url={getCertificateCreationEmail()} />
            </ActionPanel>
          }
        />
      </List.Section>
    </List>
  );
}
