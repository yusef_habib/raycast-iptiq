import { Action, Color, Icon } from "@raycast/api";
import { SearchHistory } from "./utils";

export function DeleteHistory(props: { setData: (data: SearchHistory[]) => void }) {
  return (
    <Action
      title="Delete All History"
      icon={{ source: Icon.Trash, tintColor: Color.Red }}
      onAction={() => props.setData([])}
      style={Action.Style.Destructive}
    />
  );
}
