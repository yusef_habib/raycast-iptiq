import { Action } from "@raycast/api";
import { SearchHistory } from "./utils";

function isLikePolicy(policyId: string) {
  return policyId.length >= 5;
}

function getHeimdallLink(env: string, policyId: string) {
  const domain =
    env === "prod"
      ? "prod.p.eu1.caramelspec.com"
      : env === "prep"
        ? "prep.d.eu1.caramelspec.com"
        : "dint.d.eu1.caramelspec.com";
  return isLikePolicy(policyId) ? `https://${domain}/heimdall/policy/${policyId}` : `https://${domain}/heimdall`;
}

export function OpenHeimdallLink(props: {
  search: string;
  data: SearchHistory[];
  env: string;
  setData: (data: SearchHistory[]) => void;
}) {
  return (
    <Action.OpenInBrowser
      url={getHeimdallLink(props.env, props.search)}
      title="Open on Heimdall"
      onOpen={() => {
        if (isLikePolicy(props.search)) {
          const newSearch = { value: props.search, date: new Date().toISOString() };

          const existingDataWithoutDuplicates = props.data.filter((d) => d.value !== props.search);
          props.setData([...existingDataWithoutDuplicates, newSearch]);
        }
      }}
    />
  );
}
